# uni-notes
[![pipeline status](https://gitlab.com/drsh0/uni-notes/badges/master/pipeline.svg)](https://gitlab.com/drsh0/uni-notes/commits/master)

A repo with a pipeline that converts all markdown files in any directories named `notes` to produce a pdf in `notes/pdf`. I am sure I will refine this further. Credit to script authors has been given. 

## TODO
- [ ] configure CI/CD to serve the output pdfs on a web page
- [ ] tweak pdf output, experimient with pandoc pdf

### Link Dump / Reference

* https://www.isovera.com/blog/devops-presentations-revealjs-markdown-pandoc-gitlab-ci
* https://hub.docker.com/r/ntwrkguru/pandoc-gitlab-ci/
* https://github.com/Grayda/gitlab-doc-builder/blob/master/.gitlab-ci.yml

