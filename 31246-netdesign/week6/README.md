# Lab 6

![week6-1](week6-1.png)


## Task 1: Examine the network requirements.

Examine the network requirements and answer the questions below. Keep in mind that IP addresses will be needed for each LAN interface on each router.

1. How many subnets are needed?

    6 subnets (3 for local networks, 3 for PPP networks between routers)

2. What is the maximum number of IP addresses that are needed for a single subnet?

    122 IPs (120 + 2 for BC and Network IP)

3. How many IP addresses are needed for each of the LANs?

    126 Usable IPs 

4. How many IP addresses are needed for all of the connections between routers?

    6 IPs 

5. What is the total number of IP addresses that are needed?

    396 IPs 

## Task 2: Design a hierarchical IP addressing scheme


1. What is the subnet mask for each subnetwork?
    
    /30 for PPP
    /25 for LANs

2. How many usable IP addresses are there for each subnetwork?

    /30 - 2 IPs
    /25 - 126 IPs


## Task 7: Reflection

1. How many IP addresses in 192.168.8.0/22 are wasted in this design?

    

2. What change(s) could be made to this IP addressing scheme to be more efficient?

    **Variable-length subnet masks** (VLSM) should be utilised to allocate a /30 subnect to the PPP links rather than a 128 IP block. 

3. Would route summarization have occurred in a non-hierarchical design?

