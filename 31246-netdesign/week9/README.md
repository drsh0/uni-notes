# Part A. Placing Wireless Access Points

## Step 1: Conduct the wireless site survey.

The PC is associated with **AP 1**

The wireless client was moved across the building to test coverage. Results are recorded in the image below. All other extremities of the building were out of range for wireless coverage. 

![img1](assets/layout.png)

## Step 2: Relocate the access points to their optimal location.

**Move the access points to assure that all areas inside the building have wireless coverage and that coverage outside of the building is minimized.**

![img2](assets/coverage.png)


## Step 3: Reflection

> If the coverage shown in the Physical view represents the maximum power output of the access point, could the building be 
covered by a single access point?

It cannot cover the entire building as there are signal deadspots. This was seen when performing the site survey. Multiple access points should be used. 


>On real equipment, what else could be done to minimize coverage outside of the building?

* Reducing the power output

* Using directional antennas compared to omni-directional

* Tweak beamforming if using 802.11n

>What types of problems would you encounter when you have overlapping coverage of access points using the same channel?

Efficiency of wireless communication would be greatly reduced due to **CSMA/CA**. It would cause interference and would lower speeds. This is why it's best to choose non overlapping channels such as channel 1, 6, and 11. 

# Part B. Adding Wireless to a WAN Connection

* Step 1 - 3 were already completed with the supplied packet tracer file

* After adding the wifi AP, IP config was required on the router, the AP, and the PC. 

* All pings were successful

* ACLs were created as per activity instructions. To apply the ACL to the interface the following command was issued on R2 f0/0 conf t > int - `ip access-group 100 out`