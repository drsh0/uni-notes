# Lab 4

<!-- MarkdownTOC levels="1,2,3" autolink="true"  -->

- [Part A: Exploring Access Layer Functions](#part-a-exploring-access-layer-functions)
	- [Step 1: Connect the Access Layer switch](#step-1-connect-the-access-layer-switch)
	- [Step 2: Configuration](#step-2-configuration)
	- [Step 3: Verify Connectivity](#step-3-verify-connectivity)
	- [Step 4: Reflection](#step-4-reflection)
- [Part B: Creating Modular Block Diagrams](#part-b-creating-modular-block-diagrams)
	- [Step 1: Group devices](#step-1-group-devices)
	- [Step 2: Tabulate](#step-2-tabulate)
	- [Step 3: Reflection](#step-3-reflection)

<!-- /MarkdownTOC -->

## Part A: Exploring Access Layer Functions
### Step 1: Connect the Access Layer switch

```

<Sales 2> ------------------ Fa0/2
                                >-- [Access 1B] ----<F0/1>---- [Dist 1]
<Marketing 2> -------------  Fa0/3


```

### Step 2: Configuration

#### a. Using the CLI on switch Access1B, configure the interface that connects to router Distribution1 to carry traffic for all VLANs.

```
> enable
> conf terminal
> int f0/1
> switchport mode trunk

```


#### b. Using the CLI on switch Access1B, configure the interface that connects to PC Sales2 to carry traffic for only VLAN 11.

```
> int f0/2
> switchport access vlan 11

```


#### c. Using the CLI on switch Access1B, configure the interface that connects to PC Marketing2 to carry traffic for only VLAN 21.

```
> int f0/3
> switchport access vlan 21

```

### Step 3: Verify Connectivity

All connectivity tests passed without any issues. Under simulation mode, the path of the packet was observed. `Sales2` contacted the DNS server at `192.168.0.4` first to obtain name resolution. After the DNS reply, `Sales2` was able to contact the correct web server at `192.168.0.3`. 

### Step 4: Reflection

#### a) Why are the Sales and HR servers connected to the network at the Access Layer?

Sales and HR servers will primarily be accessed by internal networks (this is an assumption). As a result, the access layer is sensible as it allows both easier local access and is accessible via VLANs throughout the organisation.  This decision could also have been made to ensure that web servers and any other external facing servers in the data center layer are given the highest amount of bandwidth and QoS possible. 

#### b) If you wanted to restrict access to the HR server, at which hierarchical network layer would you place the necessary configuration

This config would be placed and performed on the distribution layer through access control lists (ACLs). 

Generally, the core network layer should only be interested in speed, latency, the lowest amount of packet processing possible. Similarly, the access network layer is mostly comprised of layer 2 devices with a LAN and restrict access. 

## Part B: Creating Modular Block Diagrams

### Step 1: Group devices

![diagram](modular_diagram.PNG)

### Step 2: Tabulate
|                 Client Devices                	|      Access Layer Devices      	| Dist. Layer Devices 	| Core Layer Devices 	|
|:---------------------------------------------:	|:------------------------------:	|:-------------------:	|:------------------:	|
| PCs Printers Network + Business Server FC WAP 	| FC ASW 1 FC ASW 2 ProductionSW 	|  FC CPE 1 FC CPE 2  	|         N/A        	|

### Step 3: Reflection

#### a. Why is it important to group devices by their role within the network design hierarchy to analyse an existing network? 

* This allows for evaluation on each level as to whether current business needs are being met. For example, if sales productivity has decreased due to latency issues, then a network admin could have have a look at whether the devices on the access layer are performing up to spec. 

* Grouping allows a network admin to look at networks layer by layer rather than being overwhelmed with the amount of network devices to evaluate. 

* Most importantly, division by roles is required to adhere to Cisco's borderless network guidelines and general good practice regarding campus and enterprise networks.  

#### b. What is an advantage of describing a network using a modular block diagram instead of in a narrative fashion? 

* Allows for better troubleshooting and planning as devices and layers can be modified to how the network hierarchy will be affected. 
* Allows for identification if more redundancy is required by tracing packet traversals
* Less overwhelming as single network segments can be focused on
* Works better for the layered approach that network architects should be undertaking