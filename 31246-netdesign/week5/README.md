# Lab 5

## Part A: Examining WAN Connections

![partA](lab5-A.png)

### a.	In what situations would it be beneficial to use the various show commands?
### b.	What beneficial information can be obtained from the various show commands?

	show running-config

To check if device configuration errors exist e.g. entering the wrong frame relay mode or encapsulation

	show ip int brief

This is especially useful to check whether interfaces are configured correctly, addressed according to the IP scheme and most importantly whether they are currently up or down. 

	show frame-relay *

The various `frame-relay` commands allow a network adminstrator to examine how frame relay networks have been configured, and if they are configured properly to allow communication. 

	frame-relay lmi

**Local Management Interface (LMI)** displays whether both ends are communicating and auto-configuring frame relays properly. We are looking for successful exchanges. For example `Invalid Status Message 0` may indicate that there are no obvious errors with frame relay config. 

	frame-relay map

Indiciates whether the serial connection is up and to obtain more information about the frame relay connection. For example the assigned IP address, and encapsulation type. 

	frame-relay pvc

More information about **permanent virtual connection** interfaces. Provides further information about statistics, and traffic shaping.


#### Example output (Branch 1)

```
Branch1#show frame-relay map 
Serial0/0/0 (up): ip 192.168.4.1 dlci 201, dynamic, 
              broadcast, 
              CISCO, status defined, active
Branch1#show frame-relay p
Branch1#show frame-relay pvc 

PVC Statistics for interface Serial0/0/0 (Frame Relay DTE)
DLCI = 201, DLCI USAGE = LOCAL, PVC STATUS = ACTIVE, INTERFACE = Serial0/0/0

input pkts 14055       output pkts 32795        in bytes 1096228
out bytes 6216155      dropped pkts 0           in FECN pkts 0
in BECN pkts 0         out FECN pkts 0          out BECN pkts 0
in DE pkts 0           out DE pkts 0
out bcast pkts 32795   out bcast bytes 6216155

Branch1#show frame-relay lmi
LMI Statistics for interface Serial0/0/0 (Frame Relay DTE) LMI TYPE = CISCO
 Invalid Unnumbered info 0      Invalid Prot Disc 0
 Invalid dummy Call Ref 0       Invalid Msg Type 0
 Invalid Status Message 0       Invalid Lock Shift 0
 Invalid Information ID 0       Invalid Report IE Len 0
 Invalid Report Request 0       Invalid Keep IE Len 0
 Num Status Enq. Sent 79        Num Status msgs Rcvd 78
 Num Update Status Rcvd 0       Num Status Timeouts 16
```

## Part B: 

### Reflection

#### How could using simulation software such as Packet Tracer be beneficial to network personnel?
* Allow testing of networks without expensive and time consuming physical testing
* Test modules for a growing network and get feedback from all teams involved in the decision making process

#### What are some limitations to using simulation software such as Packet Tracer?

* There may be bugs
* It is limited to what has been included in the software and simulations
* It is vendor specific