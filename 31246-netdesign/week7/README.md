<!-- MarkdownTOC -->

- [Lab 7](#lab-7)
	- [Part A. Configuring a Multirouter EIGRP Network](#part-a-configuring-a-multirouter-eigrp-network)
		- [VLSM Addressing](#vlsm-addressing)
		- [EIGRP Configuration](#eigrp-configuration)
			- [R1](#r1)
			- [R2](#r2)
			- [R3](#r3)
	- [Part B. Testing Redundancy in the Network Design](#part-b-testing-redundancy-in-the-network-design)
		- [Step 1: Verify the configuration and operation of EIGRP](#step-1-verify-the-configuration-and-operation-of-eigrp)
			- [tracert between `PC1`  `PC2`:](#tracert-between-pc1-pc2)
			- [Example running-config for `R1`:](#example-running-config-for-r1)
		- [Step 2: Verify the configuration and operation of Spanning Tree Protocol:](#step-2-verify-the-configuration-and-operation-of-spanning-tree-protocol)
			- [running-config `S1`:](#running-config-s1)
			- [spanning-tree vlan 1 `S1`:](#spanning-tree-vlan-1-s1)
		- [Step 3: Simulate a failure in the switched portion of the network, verify, and observe recovery](#step-3-simulate-a-failure-in-the-switched-portion-of-the-network-verify-and-observe-recovery)
			- [Ping before and during break in connection:](#ping-before-and-during-break-in-connection)
			- [Ping after a short time:](#ping-after-a-short-time)
			- [Spanning-tree after `S1` and `S3` link broken::](#spanning-tree-after-s1-and-s3-link-broken)
			- [Diagram:](#diagram)
		- [Step 4: Simulate a failure in the routed portion of the network, verify, and observe recovery](#step-4-simulate-a-failure-in-the-routed-portion-of-the-network-verify-and-observe-recovery)
			- [From `PC` ping to `PC2`:](#from-pc-ping-to-pc2)
			- [tracert from `PC1` before link failure:](#tracert-from-pc1-before-link-failure)
			- [tracert from `PC1` after R2/R3 failure:](#tracert-from-pc1-after-r2r3-failure)
			- [Router `R1` IP route info:](#router-r1-ip-route-info)
			- [Diagram:](#diagram-1)

<!-- /MarkdownTOC -->

# Lab 7

## Part A. Configuring a Multirouter EIGRP Network 

### VLSM Addressing

Examine the network requirements and answer the questions below. Keep in mind that IP addresses will be needed for each LAN interface on each router.

a. How many subnets are needed? 

  	6 in total (9 including PtP)

b. What is the maximum number of IP addresses that are needed for a single subnet? 

  	This varies as **VLSM** will be used. Maximum is 8,000. 

c. How many IP addresses are needed for the R1 LAN 1? 

  	8002 (1 + 1 for BC and network address)

d. How many IP addresses are needed for the R1 LAN 2?

  	4002 

e. How many IP addresses are needed for the R2 LAN 1? 

  	2002

f. How many IP addresses are needed for the R2 LAN 2? 

  	1002

g. How many IP addresses are needed for the R3 LAN 1? 

  	502 

h. How many IP addresses are needed for the R3 LAN 2? 

  	202

i. What is the total number of IP addresses that are needed? 

  	15,712

j. What is the total number of IP addresses that are available in the 172.16.0.0/16 network?

  	65,534 IPs (Class B)

k. Can the network addressing requirements be met using the 172.16.0.0/16 network? 

  	**Yes**, Total IPs available in 17.16.0.0 > Total IPs required by LANs



| Name     | Network Address | Subnet Mask   | CIDR | First Usable | Last Usable   | BC Address    |
| -------- | --------------- | ------------- | ---- | ------------ | ------------- | ------------- |
| R1 LAN 1 | 172.16.0.0      | 255.255.224.0 | /19  | 172.16.0.1   | 172.16.31.254 | 172.16.31.255 |
| R1 LAN 2 | 172.16.32.0     | 255.255.240.0 | /20  | 172.16.32.1  | 172.16.47.254 | 172.16.47.255 |
| R2 LAN 1 | 172.16.48.0     | 255.255.248.0 | /21  | 172.16.48.1  | 172.16.55.254 | 172.16.55.255 |
| R2 LAN 2 | 172.16.56.0     | 255.255.252.0 | /22  | 172.16.56.1  | 172.16.59.254 | 172.16.56.255 |
| R3 LAN 1 | 172.16.60.0     | 255.255.254.0 | /23  | 172.16.60.1  | 172.16.61.254 | 172.16.61.255 |
| R3 LAN 2 | 172.16.62.0     | 255.255.255.0 | /24  | 172.16.62.1  | 172.16.62.254 | 172.16.62.255 |


### EIGRP Configuration

#### R1
```
router eigrp 1
network 172.16.0.0 0.0.31.255
network 172.16.32.0 0.0.15.255
network 209.165.202.0 0.0.0.3
network 209.165.202.4 0.0.0.3
no auto-summary
```

#### R2

```
router eigrp 1
network 172.16.48.0 0.0.7.255
network 172.16.56.0 0.0.3.255
network 209.165.202.8 0.0.0.3
network 209.165.202.0 0.0.0.3
no auto-summary
```

#### R3

```
router eigrp 1
network 172.16.60.0 0.0.1.255
network 172.16.62.0 0.0.0.255
network 209.165.202.8 0.0.0.3
network 209.165.202.4 0.0.0.3
no auto-summary
```


## Part B. Testing Redundancy in the Network Design


### Step 1: Verify the configuration and operation of EIGRP


* [x] PC1 - Able to ping all networks?
* [x] All routers - can be telnet into?
* [x] Confirm PC1 <-> PC2 network routes OK?

#### tracert between `PC1` <-> `PC2`:

```
C:\>tracert 172.18.8.10

Tracing route to 172.18.8.10 over a maximum of 30 hops: 

  1   0 ms      0 ms      0 ms      172.18.4.2
  2   0 ms      0 ms      0 ms      172.18.0.14
  3   1 ms      1 ms      0 ms      172.18.0.18
  4   0 ms      1 ms      1 ms      172.18.8.10

Trace complete.
```	

#### Example running-config for `R1`:

```
R1#show running-config 
Building configuration...

Current configuration : 830 bytes
!
version 12.3
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname R1
!
!
!
enable secret 5 $1$iHlt$EstGFIDNQH4v3oLq.4hLi1
!
!
!
!
!
!
ip cef
no ipv6 cef
!
!
spanning-tree mode pvst
!
!
!
interface FastEthernet0/0
 ip address 172.18.4.1 255.255.255.0
 duplex auto
 speed auto
!
interface FastEthernet0/1
 ip address 172.18.0.5 255.255.255.252
 duplex auto
 speed auto
!
interface Serial0/1/0
 no ip address
 clock rate 2000000
 shutdown
!
interface Serial0/1/1
 no ip address
 clock rate 2000000
 shutdown
!
interface Vlan1
 no ip address
 shutdown
!
router eigrp 1
 network 172.18.0.0
 no auto-summary
!
ip classless
!
ip flow-export version 9
!
!
!
line con 0
!
line aux 0
!
line vty 0 4
 password cisco
 login
!
!
!
end
```

### Step 2: Verify the configuration and operation of Spanning Tree Protocol:

* [x] Can telnet into S1?
* [x] Spanning-tree operating @ VLAN 1?
* [x] The above same for S2 and S3?

#### running-config `S1`:

```
S1#show running-config 
Building configuration...

Current configuration : 1243 bytes
!
version 12.2
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname S1
!
enable secret 5 $1$mCR9$rbIqqWZ0GIo7QMQdJY7t01
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
!
interface FastEthernet0/4
 switchport mode trunk
!
interface FastEthernet0/5
 switchport mode trunk
!
!
interface Vlan1
 ip address 172.18.4.3 255.255.255.0
!
ip default-gateway 172.18.4.1
!
!
!
line con 0
!
line vty 0 4
 password cisco
 login
line vty 5 15
 password cisco
 login
!
!
!
end
```

#### spanning-tree vlan 1 `S1`:

```
S1#show spanning-tree vlan 1
VLAN0001
  Spanning tree enabled protocol ieee
  Root ID    Priority    32769
             Address     0001.439D.D53B
             This bridge is the root
             Hello Time  2 sec  Max Age 20 sec  Forward Delay 15 sec

  Bridge ID  Priority    32769  (priority 32768 sys-id-ext 1)
             Address     0001.439D.D53B
             Hello Time  2 sec  Max Age 20 sec  Forward Delay 15 sec
             Aging Time  20

Interface        Role Sts Cost      Prio.Nbr Type
---------------- ---- --- --------- -------- --------------------------------
Fa0/5            Desg FWD 19        128.5    P2p
Fa0/3            Desg FWD 19        128.3    P2p
Fa0/4            Desg FWD 19        128.4    P2p
Fa0/2            Desg FWD 19        128.2    P2p
Fa0/1            Desg FWD 19        128.1    P2p
```

### Step 3: Simulate a failure in the switched portion of the network, verify, and observe recovery

* [x] Ping `Discovery` server from `PC1` sucess?
* [x] Simulate failure between `S1` and `S3`?
* [x] Is pinging successful when retrying?
* [x] Telnet to switches and view spanning-tree config for vlan 1


#### Ping before and during break in connection:

```
Reply from 172.18.4.25: bytes=32 time<1ms TTL=128
Reply from 172.18.4.25: bytes=32 time<1ms TTL=128
Reply from 172.18.4.25: bytes=32 time<1ms TTL=128
Reply from 172.18.4.25: bytes=32 time=1ms TTL=128
Request timed out.
Request timed out.
Request timed out.
```

#### Ping after a short time:

```
C:\>ping -n 1000 172.18.4.25

Pinging 172.18.4.25 with 32 bytes of data:

Request timed out.
Request timed out.
Reply from 172.18.4.25: bytes=32 time=1ms TTL=128
Reply from 172.18.4.25: bytes=32 time<1ms TTL=128
Reply from 172.18.4.25: bytes=32 time<1ms TTL=128
```


#### Spanning-tree after `S1` and `S3` link broken::

```
S1#show spanning-tree vlan 1
VLAN0001
  Spanning tree enabled protocol ieee
  Root ID    Priority    32769
             Address     0001.439D.D53B
             This bridge is the root
             Hello Time  2 sec  Max Age 20 sec  Forward Delay 15 sec

  Bridge ID  Priority    32769  (priority 32768 sys-id-ext 1)
             Address     0001.439D.D53B
             Hello Time  2 sec  Max Age 20 sec  Forward Delay 15 sec
             Aging Time  20

Interface        Role Sts Cost      Prio.Nbr Type
---------------- ---- --- --------- -------- --------------------------------
Fa0/3            Desg FWD 19        128.3    P2p
Fa0/4            Desg FWD 19        128.4    P2p
Fa0/2            Desg FWD 19        128.2    P2p
Fa0/1            Desg FWD 19        128.1    P2p
```

#### Diagram:

```mermaid
graph LR;
  PC1---S1;	
  S1---S2;
  S2---S3;
  S3---Discovery;
  S3-.disconnected.-S1;

```

### Step 4: Simulate a failure in the routed portion of the network, verify, and observe recovery

* [x] Ping `PC2` from `PC1` sucess?
* [x] Simulate failure between `R2` and `R3`?
* [x] Is pinging successful when retrying?
* [x] Trace route before and after failure?
* [x] Telnet to router and view routing table?

#### From `PC` ping to `PC2`:

```

C:\>ping -n 1000 172.18.8.10

Pinging 172.18.8.10 with 32 bytes of data:

Reply from 172.18.8.10: bytes=32 time=3ms TTL=125
Reply from 172.18.8.10: bytes=32 time=1ms TTL=125
Reply from 172.18.8.10: bytes=32 time=1ms TTL=125
```

#### tracert from `PC1` before link failure:

**NOTE** - notice how the hop is sucessfully made over `172.118.4.2` (R2)

```
C:\>tracert 172.18.8.10

Tracing route to 172.18.8.10 over a maximum of 30 hops: 

  1   1 ms      0 ms      0 ms      172.18.4.2
  2   0 ms      0 ms      0 ms      172.18.0.14
  3   0 ms      1 ms      1 ms      172.18.0.18
  4   2 ms      10 ms     0 ms      172.18.8.10
```

#### tracert from `PC1` after R2/R3 failure:

**NOTE** - notice how the hop is now made over `172.118.4.1` (R1)

```
C:\>tracert 172.18.8.10

Tracing route to 172.18.8.10 over a maximum of 30 hops: 

  1   0 ms      1 ms      0 ms      172.18.4.2
  2   0 ms      0 ms      0 ms      172.18.4.1
  3   0 ms      0 ms      1 ms      172.18.0.6
  4   1 ms      0 ms      2 ms      172.18.0.18
  5   0 ms      0 ms      1 ms      172.18.8.10
```

#### Router `R1` IP route info:

```
R1#show ip route
Codes: C - connected, S - static, I - IGRP, R - RIP, M - mobile, B - BGP
       D - EIGRP, EX - EIGRP external, O - OSPF, IA - OSPF inter area
       N1 - OSPF NSSA external type 1, N2 - OSPF NSSA external type 2
       E1 - OSPF external type 1, E2 - OSPF external type 2, E - EGP
       i - IS-IS, L1 - IS-IS level-1, L2 - IS-IS level-2, ia - IS-IS inter area
       * - candidate default, U - per-user static route, o - ODR
       P - periodic downloaded static route

Gateway of last resort is not set

     172.18.0.0/16 is variably subnetted, 5 subnets, 2 masks
C       172.18.0.4/30 is directly connected, FastEthernet0/1
D       172.18.0.12/30 [90/30720] via 172.18.0.6, 00:03:32, FastEthernet0/1
                       [90/30720] via 172.18.4.2, 00:03:32, FastEthernet0/0
D       172.18.0.16/30 [90/2172416] via 172.18.0.6, 00:54:56, FastEthernet0/1
C       172.18.4.0/24 is directly connected, FastEthernet0/0
D       172.18.8.0/24 [90/2174976] via 172.18.0.6, 00:54:52, FastEthernet0/1
```

#### Diagram:

```mermaid
graph LR;
  PC1---S1;	
  S1---R1;
  S1---R2;
  R1---R3;
  R2-.disconnected.-R3;
  R3===R4;
  R4---S4;
  S4---PC2;

```