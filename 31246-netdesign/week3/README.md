# Lab 3

## Part A. Investigating Failure Domains 

[Picture of topology](topo1.png)


### Reflection

#### **Note that the star topology does not have redundant links between switches. Why are all of the link lights green in this topology?**

Star toplogies rely on a central node to facilitate network communication. Within the packet tracker activity, the topology being empoyed is an **extended star topology**. By using additional repeaters or switches, the network can be easily extended physically. With mesh and partial mesh topologies, there are redundant links that need to be turned off for demonstration purposes. However, in a star toplogy, there are no redundant links as such and therefore all devices and their links remain on and active. 

If the central switch, *Star1*, was to fail, then the network will not be able to communicate with the other hosts.  

#### How was the effect of the network failure in the star topology different from the partial mesh and full mesh topologies? 

The key difference is that both mesh networks survived a failure in the network by being able to communicate with hosts. The star topology on the other hand did not. 

#### What effect did the removal of the link in the star topology have on the hosts that were attached to the Star2 switch? 

The hosts continued to stay connected to *Star2*, however the hosts were unable to communicate outside their own local network. 

## Part B. Observing and Recording Server Traffic 

[Picture of topology](topo2.png)

### Reflection

**No of hops between intermediate devices:**

HR - 3

Sales - 5

Web - 4

#### Based on your observations, what would be two advantages of putting all of the servers in the server farm? 

* **Faster Access + Divergence** - The server farm uses the core layer as it's backbone. The core layer is designed to offer fast transport, is made to be reliable and also have low latency. This would increase the overall availability of the servers throughout the network

* **Better security** - Rather than having servers present in the local network, it is better to isolate them in a seperate physical area. These servers could be under the same VLAN but the distribution layer would then offer access control. For example, you would not necessarily want a temporary employee to be able to physically access the server or attempt to manage it.    