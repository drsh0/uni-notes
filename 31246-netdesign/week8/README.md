<!-- MarkdownTOC -->

- [IoT Prototype](#iot-prototype)
	- [Part 1: The Topology](#part-1-the-topology)
	- [Part 2: Using te System](#part-2-using-te-system)
	- [Part 3: Challenge Questions](#part-3-challenge-questions)
- [Smart Home](#smart-home)
	- [Part 1: Explore the Smart Home](#part-1-explore-the-smart-home)
		- [Step 1: Understanding the devices that comprise the smart home](#step-1-understanding-the-devices-that-comprise-the-smart-home)
		- [Step 2: Interacting With the Smart Home](#step-2-interacting-with-the-smart-home)
	- [Part 2: Fog Computing in the Smart Home](#part-2-fog-computing-in-the-smart-home)
		- [Step 1: Run the Classic Car](#step-1-run-the-classic-car)

<!-- /MarkdownTOC -->


# IoT Prototype

## Part 1: The Topology

**Click the Run button to execute the program. What happens?**

`main.py` starts and begins to output return values

**How often are door status updates sent to the Internet server?**

1000 ms as indicated by `delay(1000);`

**What is the IP address and port of the server?**

209.165.201.2:81

**What are the variable names used to store the server IP address and server port number?**

`serverIP` and `serverPort`

**What Python modules were imported into the code?**

```python
from tcp import * # communicate over TCP/IP socket
from time import * # 1000ms
from gpio import * # interact with sensors attached to GPIO
```

## Part 2: Using te System

**What is displayed in the Smartphone?**

Status of the garage door; it is currently open (red highlight)

**Based on what you have learned in this course, what would be the role of the Internet server used in this Packet Tracer Prototype?**

The internet server allows the user to access a URI over the internet to see the status of their garage door. If there was no web server, the user would have to read the text file manually over `ssh`, `ftp` or other protocols.

**The system is designed to allow any device that is connected to the Internet to use it. Try opening the door status page from the Home PC and from the Home Laptop.**

The home pc can access the garage door status webapge. Benefits of accessing the system within the house include:

* Locally manage and administer the garage door and the SBC; increased security

* Allow future integrations with other IoT and smart home devices through APIs and triggers e.g. without sending that data over the internet 

* Faster access without having to rely on internet access

## Part 3: Challenge Questions

**Locate the Multilayer Switch. What is its function in this prototype?**

Seperate access using VLANs and perform RIP dynamic routing. Provide switching functionality stemming from ISP internet connection


**The system currently doesn’t store garage door status but it could be a good future feature. Can you think of one benefit of storing the door status over time?**

Storing garage door data either in a file, database, or via an API would be beneficial for security and for historical purposes. It would allow a security system to create patterns as to when the garage door is normally open. When it is open outside the regular pattern, a notification can be sent to the owner. 

Storage of garade door data would be useful to perform frequency analysis at EOY. This can be useful in making decisions about the garage door in the future. 

**Can you describe, as an overview, what changes would have to happen for the current system to track the status?**

I would add a timestamp component to the python script that could be stored in a `csv` file. There are various open source frameworks available such as Express that could then be used to track the data and present it using javascript graphing or data libraries. 

Optionally, an API can be introduced using Express to control the door and track it over time using a `poll` function. 

**What crucial IoT feature is missing in this prototype?**

The ability to control the garage door remotely i.e. opening and closing the door remotely. Additionally, a notification system to inform the owner that the door is now open/closed via API.



# Smart Home

## Part 1: Explore the Smart Home

### Step 1: Understanding the devices that comprise the smart home

**Two coaxial cables leave the coaxial splitter in the topology shown. Which devices does the coaxial cable connect to?**

The splitter connects a cable modem (data signal), smart window, and TV (video signal)

**The cable modem is the interface between the ISP’s network and the home’s network. To which devices does the cable modem connect to?**

Cable modem connects to the home gateway.

**List all home devices connected to the Home Gateway**

By visiting `192.168.25.1` (gateway address) I was able to find a list of connected devices:

* Smoke Detector

* Smart Alarm

* Garage Door x2

* Smart Window

* Smart Sprinkler

* Smart Water Meter

* Temperature Meter

* Smart Coffee Maker

* Smart Fan

* Smart Lamp 

* Smart Solar Panel

* Tablet + Smartphone

Total: 14 devices

### Step 2: Interacting With the Smart Home

**Because all smart devices connect to the Home Gateway which hosts a web-based interface, tablets, smartphones, laptops or desktop computers can be used to interact with the smart devices.**

**What is displayed when visiting gateway IP?**

A list of IoT devices connected along with toggles for each device. 

**Smart door: Was the door locked? How do you know?**

Yes, the light turned to red after pressing *lock*. 

**Smoke detector: Click the smoke detector in the browser to expand the section. What is the smoke level reading provided by the smoke detector? Can the smoke detector be controlled?**

Smoke level reading is 0. The smoke detector cannot be controlled. 

## Part 2: Fog Computing in the Smart Home

Fog computing is when the edge network devices are used to carry out most of the computational work compared to cloud based computing.

### Step 1: Run the Classic Car

**After turning the smart car on: What happens to the air inside the house with the car running inside the garage?**

Air quality inside decreases, temperature increases, smoke levels increase

**What happens to air inside the house after the MCU opens the doors and window, and start the fan?**

Air quality increases, temperature decreases, smoke levels drop

**Does the MCU close the doors and window, and stop the fan?**

No

**While still monitoring the levels, stop the classic car’s engine by holding the Alt key and clicking the classic car. What happens to air quality inside the house after the engine is stopped?**

Air quality increases substantially. 


**What happens to the doors, window and fan?**

They are automatically shut 