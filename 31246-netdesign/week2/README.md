# Lab 2
## Part A. Observing Web Requests

### Notes 
* The HTTP server is reachable via port `80` as is standard
* When creating the complex PDU for simulation, the source port is `8000`
  * this is standard for the TCP protocol as it utilises sockets and multiplexing to deliver data between servers and multiple clients. 
* TCP handshake was also observed
* The PC contacted the DNS server `x.x.x.50` which has A records of `ciscolearn.web.com`. This could have been set up on the gateway instead but a seperate DNS server offers better control

## Part B. Using the Ping Command

### Notes
* PC 2 is unable to access `www.cisco.pka`
* However, by using `nslookup www.cisco.pka` we are able to find an IP address of the web server
* Checking DNS server settings on client reveals that the DNS server address is incorrect; changing this fixes the website resolution issue
* In this scenario, a secondary DNS server should have been entered to ensure a fallback is present for domain name resolution
